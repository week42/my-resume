import 'package:flutter/material.dart';

Row _buildTitleRow(IconData icon, String title) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Icon(
        icon,
        color: Colors.black,
      ),
      Container(
        padding: EdgeInsets.only(left: 8),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w400,
          ),
        ),
      )
    ],
  );
}

Text _buildDetailText(String detail) {
  return Text(
    detail,
    style: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w400,
    ),
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget profileSection = Container(
      padding: const EdgeInsets.fromLTRB(32,32,32,5),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                _buildTitleRow(Icons.account_circle_sharp, 'ชื่อ'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('สุรเชษฐ์ เรียงเล็กจำนงค์')],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                _buildTitleRow(Icons.cake, 'วันเกิดและอายุ'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('12 ตุลาคม พ.ศ.2542 อายุ 21 ปี')],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                _buildTitleRow(Icons.male, 'เพศ'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('ชาย')],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                _buildTitleRow(Icons.home, 'ที่อยู่'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    _buildDetailText(
                        '563 หมู่ 7 ต.เขาคันทรง อ.ศรีราชา จ.ชลบุรี 20110')
                  ],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                _buildTitleRow(Icons.alternate_email, 'อีเมล'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('surachet_r@outlook.com')],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                _buildTitleRow(Icons.phone, 'เบอร์โทร'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('064-4969569')],
                ))
              ],
            ),
          ),
        ],
      ),
    );
    Widget skillsSection = Container(
      padding: const EdgeInsets.fromLTRB(32,0,32,5),
            child: Row(
              children: [
                _buildTitleRow(Icons.stars, 'ทักษะที่มี'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('')],
                ))
              ],
            ),
    );
    Widget educationSection = Container(
      padding: const EdgeInsets.fromLTRB(32,0,32,5),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                _buildTitleRow(Icons.school, 'การศึกษา'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('')],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(40,0,0,5),
            child: Row(
              children: [
                _buildTitleRow(Icons.adjust, 'ระดับมัธยมศึกษา'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('โรงเรียนมารีวิทย์บ่อวิน')],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(40,0,0,5),
            child: Row(
              children: [
                _buildTitleRow(Icons.adjust, 'ระดับประถมศึกษา'),
                Expanded(
                    child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_buildDetailText('โรงเรียนสมคิดจิตต์วิทยา')],
                ))
              ],
            ),
          ),
        ],
      ),
    );
    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
            appBar: AppBar(
              title: const Text('My Resume'),
              leading: IconButton(
                  onPressed: null,
                  icon:
                      Icon(Icons.account_circle_outlined, color: Colors.white)),
            ),
            body: ListView(
              children: [
                Image.asset(
                  'images/profile.jpg',
                  width: 225,
                  height: 250,
                  fit: BoxFit.contain,
                ),
                profileSection,
                skillsSection,
                Container(
                  padding: const EdgeInsets.fromLTRB(64,0,32,5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        'images/java.png',
                        width: 150,
                        height: 75,
                        fit: BoxFit.contain,
                      ),
                      Image.asset(
                        'images/csharp.png',
                        width: 150,
                        height: 75,
                        fit: BoxFit.contain,
                      ),
                      Image.asset(
                        'images/python.png',
                        width: 150,
                        height: 75,
                        fit: BoxFit.contain,
                      ),
                      Image.asset(
                        'images/cplus.png',
                        width: 150,
                        height: 75,
                        fit: BoxFit.contain,
                      ),
                    ],
                  )
                ),
                educationSection,
              ],
            )));
  }
}
